#include <Windows.h>
#include <gl/GL.h>
#include <cstdio>
#include <string>
#include <sstream>

#pragma comment(lib, "opengl32.lib")

__int64 prevTime                                 = 0;
__int64 currentTime                              = 0;
__int64 countsPerSec                             = 0;
float secondsPerCount                            = 0.0f;
float deltaTime                                  = 0.0f;
												 
struct Window_t									 
{												 
	unsigned int ScreenWidth                     = 0;
	unsigned int ScreenHeight                    = 0;
	const char* Title                            = nullptr;
												 
	WNDCLASS wc                                  = { 0 };
	MSG msg                                      = { 0 };
	HWND hWnd                                    = { 0 };
	HDC hDc                                      = { 0 };
	HGLRC hglrc                                  = { 0 };
	PIXELFORMATDESCRIPTOR DesiredPixelFormat     = { 0 };
	PIXELFORMATDESCRIPTOR SuggestedPixelFormat   = { 0 };
};

void HandleEvents()
{

}

void Update()
{
	//printf("%F\n", deltaTime);

	//std::stringstream ss;
	//ss << "Frame Time: " << deltaTime;

	//SetWindowText(hWnd, ss.str().c_str());
}

void Render(HDC hDc)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	SwapBuffers(hDc);
}

LRESULT CALLBACK WindProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		} break;

		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}

#ifdef GUNN_DEBUG
int main()
{
	WinMain(GetModuleHandle(nullptr), GetModuleHandle(nullptr), nullptr, SW_SHOW);

	return(0);
}
#endif

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevinstance, LPSTR cmdLine, int nCmdShow)
{
	WNDCLASS wc = { 0 };
	wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindProc;
	wc.hInstance = hInstance;
	wc.lpszClassName = "GunngineWindowClass";
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClass(&wc))
	{
		printf("Error registering window class\n");
		return(-1);
	}

	HWND hWnd = CreateWindow("GunngineWindowClass", "Gunngine", WS_OVERLAPPEDWINDOW&~WS_MAXIMIZEBOX ^ WS_THICKFRAME | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		printf("Error creating window\n");
		return(-1);
	}

	HDC hDc = GetDC(hWnd);
	if (!hDc)
	{
		printf("Error creating device context");
		return 0;
	}

	PIXELFORMATDESCRIPTOR DesiredPixelFormat     = { 0 };
	PIXELFORMATDESCRIPTOR SuggestedPixelFormat   = { 0 };
												 
	DesiredPixelFormat.nSize                     = sizeof(PIXELFORMATDESCRIPTOR);
	DesiredPixelFormat.nVersion                  = 1;
	DesiredPixelFormat.dwFlags                   = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
	DesiredPixelFormat.cColorBits                = 32;
	DesiredPixelFormat.cAlphaBits                = 8;
	DesiredPixelFormat.iLayerType                = PFD_MAIN_PLANE;

	int SuggestedPixelFormatIndex = ChoosePixelFormat(hDc, &DesiredPixelFormat);
	DescribePixelFormat(hDc, SuggestedPixelFormatIndex, sizeof(SuggestedPixelFormat), &SuggestedPixelFormat);
	if (!SetPixelFormat(hDc, SuggestedPixelFormatIndex, &SuggestedPixelFormat))
	{
		printf("Error setting pixel format\n");
		return(-1);
	}

	HGLRC hglrc = wglCreateContext(hDc);
	if (!wglMakeCurrent(hDc, hglrc))
	{
		printf("Fail to create Renderer context!\n");
		return(-1);
	}
	ReleaseDC(hWnd, hDc);

	QueryPerformanceCounter((LARGE_INTEGER*)&prevTime);
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	secondsPerCount = 1.0f / countsPerSec;

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			currentTime = 0;
			QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);
			deltaTime = (currentTime - prevTime) * secondsPerCount * 1000;

			HandleEvents();
			Update();
			Render(hDc);

			prevTime = currentTime;
		}
	}

	return(0);
}